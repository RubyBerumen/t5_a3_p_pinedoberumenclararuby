'''
Created on 6 dic. 2020

@author: Ruby
'''
import random
import copy
import time

class NumerosAleatorios:
    def generarNumeros(self,cantidad):
        nums = []
        for i in range(cantidad):
            nums.append(random.randint(0,cantidad))
        return nums
    
class MetodosOrdenamiento:
    
    class Intercalacion:
        
        def ordenar(self,nums):
            numeros=nums.copy()
            comparaciones=0
            intercambios=0
            recorridos=0
            tiempo=0
            
            a1=[]
            a2=[]
            
            for i in range(len(nums)):
                if(i<(len(nums)/2)):
                    a1.append(numeros[i])
                else:
                    a2.append(numeros[i])
                    
            a1.sort()
            a2.sort()
            
            tInicio = time.time_ns()
            a3 : list = []
            cont : int = 0
            cont2 : int = 0
            
            recorridos+=1
            while len(a1)!=cont and len(a2)!=cont2:
                comparaciones+=1
                intercambios+=1
                if a1[cont]<=a2[cont2]:
                    a3.append(a1[cont])
                    cont+=1
                else:
                    a3.append(a2[cont2])
                    cont2+=1
                    
            recorridos+=1
            while len(a1)!=cont:
                intercambios+=1
                a3.append(a1[cont])
                cont+=1
            
            recorridos+=1
            while len(a2)!=cont2:
                intercambios+=1
                a3.append(a2[cont2])
                cont2+=1
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Intercalacion:     {tiempo}         {recorridos}         {comparaciones}    {intercambios}")
        
        
    class MezclaDirecta:
        def __init__(self):
            self.comparaciones=0
            self.intercambios=0
            self.recorridos=0
            self.tiempo=0
        
        def ordenamientoMezclaDirecta(self,array):
        
            mitad=len(array)//2
        
            if len(array)>=2:
                arregloIz=array[mitad:]
                arregloDer=array[:mitad]

                array.clear()             
                self.ordenamientoMezclaDirecta(arregloIz)
                self.ordenamientoMezclaDirecta(arregloDer)
            
                self.recorridos+=1
                while(len(arregloDer)>0 and len(arregloIz)>0):
                    self.comparaciones+=1
                    self.intercambios+=1
                    if(arregloIz[0]< arregloDer[0]):
                        array.append(arregloIz.pop(0))
                    else:
                        array.append(arregloDer.pop(0))
                       
                self.recorridos+=1
                while len(arregloIz)>0:
                    self.intercambios+=1
                    array.append(arregloIz.pop(0))
                
                self.recorridos+=1
                while len(arregloDer)>0:
                    self.intercambios+=1
                    array.append(arregloDer.pop(0))
        
            return array
        
        def llamadaMezclaDirecta(self,nums):
            numeros = nums.copy()
            
            tInicio = time.time_ns()
            mo = MetodosOrdenamiento.MezclaDirecta()
            mo.ordenamientoMezclaDirecta(numeros)
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Mezcla directa:    {tiempo}    {mo.recorridos}    {mo.comparaciones}    {mo.intercambios}")
       
        
    class MezclaNatural:
        
        def __init__(self):
            self.comparaciones=0
            self.intercambios=0
            self.recorridos=0
            self.tiempo=0
        
        def mezclaDirecta(self,array):
        
            mitad=len(array)//2
        
            if len(array)>=2:
                arregloIz=array[mitad:]
                arregloDer=array[:mitad]

                array.clear()  
            
                self.mezclaDirecta(arregloIz)
            
                self.mezclaDirecta(arregloDer)
                
                self.recorridos+=1
                while(len(arregloDer)>0 and len(arregloIz)>0):
                    self.comparaciones+=1
                    self.intercambios+=1
                    if(arregloIz[0]< arregloDer[0]):
                        array.append(arregloIz.pop(0))
                    else:
                        array.append(arregloDer.pop(0))
                self.recorridos+=1
                while len(arregloIz)>0:
                    self.intercambios+=1
                    array.append(arregloIz.pop(0))
                    
                self.recorridos+=1
                while len(arregloDer)>0:
                    self.intercambios+=1
                    array.append(arregloDer.pop(0))
        
            return array
    
        def mezclaDirecta2(self, array):
            mitad=len(array)//2
        
            if len(array)>=2:
                arregloIz=array[mitad:]
                arregloDer=array[:mitad]

                array.clear()  
            
                self.mezclaDirecta(arregloIz)
            
                self.mezclaDirecta(arregloDer)
                
                self.recorridos+=1
                while(len(arregloDer)>0 and len(arregloIz)>0):
                    self.comparaciones+=1
                    self.intercambios+=1
                    if(arregloIz[0]< arregloDer[0]):
                        array.append(arregloIz.pop(0))
                    else:
                        array.append(arregloDer.pop(0))
                self.recorridos+=1
                while len(arregloIz)>0:
                    self.intercambios+=1
                    array.append(arregloIz.pop(0))
                self.recorridos+=1
                while len(arregloDer)>0:
                    self.intercambios+=1
                    array.append(arregloDer.pop(0))
        
        
        def mezclaNatural(self, numeros):
           
            izquerdo = 0
            izq = 0
            derecho = len(numeros)-1
            der = derecho
            ordenado=False
            
            while(not ordenado):
                ordenado = True
                izquierdo =0
                self.recorridos+=1
                while(izquierdo<derecho):
                    izq=izquerdo
                    self.recorridos+=1
                    while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                        izq=izq+1
                    der=izq+1
                    self.recorridos+=1
                    while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                        der=der+1
                    if(der<=derecho):
                        self.mezclaDirecta2(numeros)
                        ordenado= False
                    izquierdo = izq
            
        
        def llamadaMezclaNatural(self,nums):
            numeros=nums.copy()
            
            tInicio = time.time_ns()
            mn = MetodosOrdenamiento.MezclaNatural()
            mn.mezclaNatural(numeros)
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Mezcla natural:    {tiempo}    {mn.recorridos}    {mn.comparaciones}    {mn.intercambios}")
            
            
class LamadaMetodos:
    
    def llamar(self, n):
        
        mo = MetodosOrdenamiento()
        mo.Intercalacion.ordenar(mo, n) 
        mo.MezclaDirecta.llamadaMezclaDirecta(mo, n)
        mo.MezclaNatural.llamadaMezclaNatural(mo, n)       

lm = LamadaMetodos()
na1 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 1000)
na2 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 10000)
na3 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 100000)
        
print("----------------------1000 numeros----------------------")
print("Metodo             Tiempo      Rec   Comp     Int")
lm.llamar(na1)
print()
print("----------------------10000 numeros---------------------")
print("Metodo             Tiempo        Rec      Comp        Int")
lm.llamar(na2)
print()
print("----------------------100000 numeros--------------------")
print("Metodo             Tiempo          Rec      Comp        Int")
lm.llamar(na3)      
        
        